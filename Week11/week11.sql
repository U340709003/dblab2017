use movie_db;

# 6. Show the thriller films whose budget is greater than 25 million$.

Select movies.title 
from movies join genres on movies.movie_id = genres.movie_id 
where budget > 25000000 and genre_name = "Thriller";

# 7. Show the drama films whose language is Italian and produced between 1990-2000.

Select movies.title 
from movies,languages,genres 
where language_name = "Italian" and genre_name = "Drama" and year between 1990 and 2000 and movies.movie_id=languages.movie_id=genres.movie_id;

Select movies.title 
from movies 
where year between 1990 and 2000 and movie_id in 
(Select genres.movie_id 
from languages join genres on languages.movie_id = genres.movie_id 
where language_name = "Italian" and genre_name = "Drama");

# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	

Select movies.title 
from movies 
where oscars > 3 and movie_id in 
(Select movie_id 
from movie_stars inner join stars on movie_stars.star_id=stars.star_id 
where star_name = "Tom Hanks");

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.

Select movies.title
from movies
where duration between 100 and 200 and movie_id in
(Select movie_id
from producer_countries join countries on producer_countries.country_id = countries.country_id
where country_name = "USA" and movie_id in 
(Select movie_id
from genres
where genre_name ="History")
);

# 10.Compute the average budget of the films directed by Peter Jackson.

select AVG(budget) 
from movies
where movie_id in
(Select movie_id from directors join movie_directors on directors.director_id = movie_directors.director_id
where director_name = "Peter Jackson");

# 11.Show the Francis Ford Coppola film that has the minimum budget.

select movies.title, budget
from movies
where movie_id in
(Select movie_id from directors join movie_directors on directors.director_id = movie_directors.director_id
where director_name = "Francis Ford Coppola")
order by budget asc
limit 1;

# 12.Show the film that has the most vote and has been produced in USA.

Select movies.title, votes
from movies 
where movie_id in
(Select movie_id
from producer_countries join countries on producer_countries.country_id = countries.country_id
where country_name = "USA")
order by votes DESC
limit 1;
