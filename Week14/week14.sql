# initial time: 5.258
#after index: 2.196
#after PK: 1.326

use unitprot;

select * from proteins;
load data local infile 'C:\\Users\\insert.txt' into table proteins fields terminated by '|';

select *
from proteins
where protein_name like "%tumor%" and unitprot_id like "%human%"
order by unitprot_id;

create index unitprot_index on proteins (unitprot_id);
drop index unitprot_index on proteins;

alter table proteins add constraint pk_proteins primary key (unitprot_id);
alter table proteins drop primary key;